var express = require('express') ;
var fs = require('fs') ;
var app = express() ;

app.get('/', function(req, res) {
	res.setHeader('Content-Type', 'text/plain') ;
	res.end('Bonjour sur vente-legumes.com') ;
}) ;

// retourne le contenu du fichier json
app.get('/produits', function(req, res) {
	res.setHeader('Content-Type', 'application/json') ;
	res.setHeader('Access-Control-Allow-Origin', '*') ;
	var readable = fs.createReadStream("produits.json") ;
	readable.on('open', function() {
		readable.pipe(res) ;
		console.log("Liste des produits retournée") ;
	}) ;
	readable.on('error', function() {res.send("[]")}) ;
});

// La liste des membres en plus élaborée
app.get('/produits/:produit', function(req, res) {
	res.setHeader('Content-Type', 'application/json') ;
	res.setHeader('Access-Control-Allow-Origin', '*') ;

	console.log("serveur node : /produits/:produit") ;
	var produitAChercher = req.params.produit ;
	var chaineProduits = fs.readFileSync("produits.json", "UTF-8") ;
	var listeProduits = JSON.parse(chaineProduits) ;
	var produits = [] ;
	for (var i=0; i<listeProduits.length; i++ ){
		console.log(listeProduits[i]) ;
		var produit = listeProduits[i].produit ;
		var prix = listeProduits[i].prix ;
		if (listeProduits[i].produit == produitAChercher || listeProduits[i].prix == produitAChercher) {
			produits.push({"produit":produit,"prix":prix});
		}
	}

	var json = JSON.stringify(produits) ;
	console.log(" -> json : " + json) ;
	res.end(json) ;
});


app.listen(8888) ;
