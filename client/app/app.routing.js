"use strict";
var router_1 = require('@angular/router');
var Composant1_component_1 = require('./Composant1.component');
var Composant2_component_1 = require('./Composant2.component');
var RechercheParMarque_component_1 = require('./RechercheParMarque.component');
var RechercheParType_component_1 = require('./RechercheParType.component');
var DetailProduit_component_1 = require('./DetailProduit.component');
var appRoutes = [
    { path: 'produits/marques', component: Composant1_component_1.Composant1Component },
    { path: 'produits/types', component: Composant2_component_1.Composant2Component },
    { path: 'rechercheParMarque/:marque', component: RechercheParMarque_component_1.RechercheParMarqueComponent },
    { path: 'rechercheParType/:type', component: RechercheParType_component_1.RechercheParTypeComponent },
    { path: 'detailProduit/:produit', component: DetailProduit_component_1.DetailProduitComponent }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map