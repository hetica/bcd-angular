"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
require('rxjs/add/operator/map');
var Composant1Component = (function () {
    function Composant1Component(http, router) {
        this.http = http;
        this.router = router;
    }
    Composant1Component.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Recherche de toutes les marques");
        this.http.get('http://web-avance:8888/produits/listeDesMarques')
            .map(function (res) { return res.json(); })
            .subscribe(function (res) { return _this.items = res; }, function (err) { return console.error(err); }, function () { return console.log('done'); });
    };
    Composant1Component.prototype.appelComposant3 = function (parametre) {
        var link = ['rechercheParMarque', parametre];
        this.router.navigate(link);
    };
    Composant1Component = __decorate([
        core_1.Component({
            selector: 'composant1',
            templateUrl: 'templates/composant1.html',
            styleUrls: ['styles/composant1.css']
        }), 
        __metadata('design:paramtypes', [http_1.Http, router_1.Router])
    ], Composant1Component);
    return Composant1Component;
}());
exports.Composant1Component = Composant1Component;
//# sourceMappingURL=Composant1.component.js.map