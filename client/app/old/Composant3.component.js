"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
require('rxjs/add/operator/map');
var Composant3Component = (function () {
    function Composant3Component(http, route) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.route.params
            .map(function (params) { return params['marque']; })
            .subscribe(function (marque) { return _this.marque = marque; });
    }
    Composant3Component.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Recherche de la marque " + this.marque);
        this.http.get('http://web-avance:8888/produits/marque/' + this.marque)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) { return _this.items = res; }, function (err) { return console.error(err); }, function () { return console.log('done'); });
    };
    Composant3Component = __decorate([
        core_1.Component({
            selector: 'composant3',
            templateUrl: 'templates/composant3.html',
            styleUrls: ['styles/composant1.css']
        }), 
        __metadata('design:paramtypes', [http_1.Http, router_1.ActivatedRoute])
    ], Composant3Component);
    return Composant3Component;
}());
exports.Composant3Component = Composant3Component;
//# sourceMappingURL=Composant3.component.js.map