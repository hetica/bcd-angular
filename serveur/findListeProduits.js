var MongoClient = require('mongodb').MongoClient ;
var assert = require ('assert') ;
var ObjectId = require('mongodb').objectID ;
var url = 'mongodb://localhost:27017/MaterielInfo' ;

var findProduits = function(db, search, callback) {
	var cursor = db.collection('Produits').find(search) ;
	cursor.each(function(err, doc) {
		assert.equal(err, null) ;
		if (doc != null) {
			// console.dir(doc)
			for (var p in doc) {
				console.log(p + " : " + doc[p]) ;
			}
		} else {
			callback() ;
		}
		console.log("\n") ;
	}) ;
}

MongoClient.connect(url, function(err, db) {
	assert.equal(null, err) ;
	findProduits(db, {}, function() {
		db.close() ;
	}) ;
}) ;

MongoClient.connect(url, function(err, db) {
	assert.equal(null, err) ;
	findProduits(db, { "type": "processeur" }, function() {
		db.close() ;
	}) ;
}) ;

