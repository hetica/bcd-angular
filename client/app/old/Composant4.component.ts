import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
    selector:    'composant4',
    templateUrl: 'templates/composant4.html',
    styleUrls:   ['styles/composant1.css']
})
export class Composant4Component {
    private http: Http;
    private route :ActivatedRoute;
    public items :any;
    public type :string;

    public constructor(http :Http, route: ActivatedRoute) {
        this.http = http;
        this.route = route;
        this.route.params
            .map(params => params['type'])
            .subscribe(type => this.type = type);
    }

    ngOnInit() {
        console.log("Recherche du type "+this.type);
        this.http.get('http://web-avance:8888/produits/type/'+this.type)
                    .map((res:Response) => res.json())
                    .subscribe(res => this.items = res,
                            err => console.error(err),
                            () => console.log('done') );
    }
}
