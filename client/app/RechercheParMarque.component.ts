import { Component, OnInit } from '@angular/core' ;
import { ActivatedRoute } from '@angular/router' ;
import { RechercheService } from './Recherche.service' ;

@Component({
    templateUrl: 'templates/rechercheParMarque.html',
    styleUrls:   ['styles/composant1.css']
})
export class RechercheParMarqueComponent {
    public items :any ;
    public marque :string ;

    public constructor(private recherche :RechercheService, private route:ActivatedRoute) {
        this.route = route;
        this.route.params
            .map(params => params['marque'])
            .subscribe(marque => this.marque = marque);
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.recherche.getJSON("marque/"+params['marque'])
                    .subscribe(res => this.items = res,
                            err => console.error(err),
                            () => console.log('done'));
        });
    }
}
