import { Component, OnInit } from '@angular/core' ;
import { ActivatedRoute } from '@angular/router' ;
import { RechercheService } from './Recherche.service' ;

@Component({
    templateUrl: 'templates/rechercheParType.html',
    styleUrls:   ['styles/composant1.css']
})
export class RechercheParTypeComponent {
    public items :any ;
    public type :string ;

    public constructor(private recherche :RechercheService, private route:ActivatedRoute) {
        this.route = route;
        this.route.params
            .map(params => params['type'])
            .subscribe(type => this.type = type);
    }

    ngOnInit() {
        // console.log("Recherche du produit de type " + this.type);
        this.route.params.subscribe(params => {
            this.recherche.getJSON("type/"+params['type'])
                    .subscribe(res => this.items = res,
                            err => console.error(err),
                            () => console.log('done'));
        });
    }
}
