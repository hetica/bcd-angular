import {Component} from '@angular/core';

@Component({
    selector:    'menu',
    templateUrl: 'templates/menu.html',
    styleUrls:   ['styles/menu.css']
})
export class MenuComponent {
    titre = 'Recherche sur les produits' ;
    item :string = 'Intel' ;

    setItem( value :string) {
      console.log("Dans Menu avec "+value) ;
      this.item = value ;
   }
}

