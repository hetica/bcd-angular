import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { routing, appRoutingProviders } from './app.routing';

import { RechercheService } from './Recherche.service'

import { MenuComponent }        from './Menu.component';
import { Composant1Component }  from './Composant1.component';
import { Composant2Component }  from './Composant2.component';
import { RechercheParMarqueComponent }  from './RechercheParMarque.component';
import { RechercheParTypeComponent }  from './RechercheParType.component';
import { DetailProduitComponent }  from './DetailProduit.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, routing ],
  declarations: [ MenuComponent,
                Composant1Component,
                Composant2Component,
                RechercheParMarqueComponent,
                RechercheParTypeComponent,
                DetailProduitComponent
                ],
  providers:    [ appRoutingProviders, RechercheService ],
  bootstrap:    [ MenuComponent ]
})
export class AppModule { }
