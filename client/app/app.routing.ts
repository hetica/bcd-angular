import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Composant1Component }  from './Composant1.component';
import { Composant2Component }  from './Composant2.component';
import { RechercheParMarqueComponent }  from './RechercheParMarque.component';
import { RechercheParTypeComponent }  from './RechercheParType.component';
import { DetailProduitComponent }  from './DetailProduit.component';

const appRoutes: Routes = [
    { path: 'produits/marques', component: Composant1Component },
    { path: 'produits/types', component: Composant2Component },
    { path: 'rechercheParMarque/:marque', component: RechercheParMarqueComponent },
    { path: 'rechercheParType/:type', component: RechercheParTypeComponent },
    { path: 'detailProduit/:produit', component: DetailProduitComponent }
];

export const appRoutingProviders: any[] = [ ];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
