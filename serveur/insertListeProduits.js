var MongoClient = require('mongodb').MongoClient ;
var assert = require('assert') ;
var ObjectId = require('mongodb').ObjectId ;
var url = 'mongodb://localhost:27017/MaterielInfo' ;

var insertDocument = function(db, callback) {
	db.collection('Produits').insert(
	[
		{'type': 'disque dur', 'reference': '000001', 'nom': 'DT01AC', 'marque': 'Toshiba', 'prix': '44.99', 'stock': true } ,
		{'type': 'disque dur', 'reference': '000002', 'nom': 'P300', 'marque': 'Toshiba', 'prix': '52.99', 'stock': true } ,
		{'type': 'disque dur', 'reference': '000003', 'nom': 'GLPT240', 'marque': 'Seagate', 'prix': '52.90', 'stock': true } ,
		{'type': 'disque dur', 'reference': '000004', 'nom': 'BarraCuda', 'marque': 'Seagate', 'prix': '54.90', 'stock': true } ,
		{'type': 'processeur', 'reference': '000005', 'nom': 'Core i7 5775C', 'marque': 'Intel', 'prix': '404.99', 'stock': true } ,
		{'type': 'processeur', 'reference': '000006', 'nom': 'Core i7 4770S', 'marque': 'Intel', 'prix': '329.75', 'stock': false } ,
		{'type': 'processeur', 'reference': '000007', 'nom': 'Core i7 4790', 'marque': 'Intel', 'prix': '318.65', 'stock': true } ,
		{'type': 'processeur', 'reference': '000008', 'nom': 'Core i7 4930K', 'marque': 'Intel', 'prix': '659.99', 'stock': true } ,
		{'type': 'processeur', 'reference': '000009', 'nom': 'FX 6300', 'marque': 'AMD', 'prix': '114.65', 'stock': true } ,
		{'type': 'processeur', 'reference': '000010', 'nom': 'FX 6350', 'marque': 'AMD', 'prix': '134.89', 'stock': true }
	]
		, function(err, result) {
			assert.equal(err, null) ;
			console.log("Insertion de documents dans la collection Produits.") ;
			callback(result) ;
		}) ;
	} ;

MongoClient.connect(url, function(err, db) {
	assert.equal(null, err) ;
	insertDocument(db, function() {
		db.close() ;
	}) ;
}) ;
