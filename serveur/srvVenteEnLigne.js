// Connexion à la base de MongoDB
var MongoClient = require('mongodb').MongoClient ;
var assert = require ('assert') ;
var ObjectId = require('mongodb').objectID ;
var url = 'mongodb://localhost:27017/MaterielInfo' ;
// express permet de faire les app.get
express = require('express') ;
var app = express() ;

MongoClient.connect(url, function(err, database) {
	assert.equal(null, err) ;
	db = database ;
	app.listen(8888) ;
}) ;

app.get('/produits/marques' , function(req, res) {
	console.log("Recherches sur les marques");
	findProduits (db, {}, function(json) {
		console.log(json);
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(json));
    });
}) ;

app.get('/produits/listeDesMarques' , function(req, res) {
	console.log("Recherches sur les marques");
	db.collection('Produits').distinct ('marque', function(err, json) {
		console.log(json);
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(json));
    });
}) ;

app.get('/produits/types' , function(req, res) {
	console.log("Recherches sur les types");
	findProduits (db, {}, function(json) {
		console.log(json);
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(json));
    });
}) ;

app.get('/produits/listeDesTypes' , function(req, res) {
	console.log("Recherches sur les types");
	db.collection('Produits').distinct ('type', function(err, json) {
		console.log(json);
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(json));
    });
}) ;

app.get("/produits/marque/:marque", function(req, res) {
    console.log("params: "+req) ;
    var marqueARechercher = req.params.marque;
    console.log("Marque à rechercher : "+marqueARechercher);
    findProduits(db, {"marque":marqueARechercher}, function(json) {
         console.log(json);
         res.setHeader('Access-Control-Allow-Origin', '*');
         res.setHeader('Content-Type', 'application/json');
         res.end(JSON.stringify(json));
    });
})

app.get("/produits/type/:type", function(req, res) {
    var typeARechercher = req.params.type;
    console.log("Type à rechercher : "+typeARechercher);
    findProduits(db, {"type":typeARechercher}, function(json) {
         console.log(json);
         res.setHeader('Access-Control-Allow-Origin', '*');
         res.setHeader('Content-Type', 'application/json');
         res.end(JSON.stringify(json));
    });
})

var findProduits = function(db, search, callback) {
	var cursor = db.collection('Produits').find(search) ;
	var res = [] ;
	cursor.each(function(err, doc) {
		assert.equal(err, null) ;
		if (doc != null) {
			res.push(doc) ;
			//console.dir(doc)
		} else { callback(res) ; }
		//console.log("\n") ;
	}) ;
} ;


