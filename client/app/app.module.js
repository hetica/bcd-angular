"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_routing_1 = require('./app.routing');
var Recherche_service_1 = require('./Recherche.service');
var Menu_component_1 = require('./Menu.component');
var Composant1_component_1 = require('./Composant1.component');
var Composant2_component_1 = require('./Composant2.component');
var RechercheParMarque_component_1 = require('./RechercheParMarque.component');
var RechercheParType_component_1 = require('./RechercheParType.component');
var DetailProduit_component_1 = require('./DetailProduit.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, app_routing_1.routing],
            declarations: [Menu_component_1.MenuComponent,
                Composant1_component_1.Composant1Component,
                Composant2_component_1.Composant2Component,
                RechercheParMarque_component_1.RechercheParMarqueComponent,
                RechercheParType_component_1.RechercheParTypeComponent,
                DetailProduit_component_1.DetailProduitComponent
            ],
            providers: [app_routing_1.appRoutingProviders, Recherche_service_1.RechercheService],
            bootstrap: [Menu_component_1.MenuComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map