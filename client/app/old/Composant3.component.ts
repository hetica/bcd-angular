import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
    selector:    'composant3',
    templateUrl: 'templates/composant3.html',
    styleUrls:   ['styles/composant1.css']
})
export class Composant3Component {
    private http: Http;
    private route :ActivatedRoute;
    public items :any;
    public marque :string;

    public constructor(http:Http, route:ActivatedRoute) {
       this.http = http;
       this.route = route;
       this.route.params
            .map(params => params['marque'])
            .subscribe(marque => this.marque = marque);
    }

    ngOnInit() {
           console.log("Recherche de la marque " + this.marque);
           this.http.get('http://web-avance:8888/produits/marque/'+this.marque)
                    .map((res:Response) => res.json())
                    .subscribe(res => this.items = res,
                            err => console.error(err),
                            () => console.log('done') );
    }
}
