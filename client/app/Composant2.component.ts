import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
    selector:    'composant2',
    templateUrl: 'templates/composant2.html',
    styleUrls:   ['styles/composant2.css']
})
export class Composant2Component {
    private http: Http ;
    private router: Router ;
    public items: any ;
    public type: string ;

    public constructor(http :Http, router: Router) {
        this.http = http;
        this.router = router ;
    }

    ngOnInit() {
        console.log('Appel du serveur pour importer les types des produits');

        this.http.get('http://web-avance:8888/produits/listeDesTypes')
                     .map((res:Response) => res.json())
                     .subscribe(res => this.items = res,
                                err => console.log(err),
                                () => console.log('done') );
    }

    public appelComposant4(parametre:string) {
        //let link = ['/produits/types', parametre] ;
        let link = ['rechercheParType', parametre];
        this.router.navigate(link) ;
    }
}
